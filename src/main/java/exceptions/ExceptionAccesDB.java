package exceptions;

import model.utils.JournalLog;

public class ExceptionAccesDB extends ExceptionHandler {

	private static final long serialVersionUID = -1499656337734035129L;

	public ExceptionAccesDB(String msg) {
		super(msg, JournalLog.ERROR);
	}
	
}
