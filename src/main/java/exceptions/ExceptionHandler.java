package exceptions;

import model.utils.JournalLog;

public class ExceptionHandler extends Exception {

	private static final long serialVersionUID = 7506055338527152123L;

	public ExceptionHandler(String msg, int i) {
		//this.getClass();
		JournalLog.addMessage(i,  msg);
		this.printStackTrace();
	}
	
}
