package model;
import java.util.HashMap;
import java.util.Map;

public class Stockeur<T extends Stockable> {
	
	//ATTRIBUTS
	private Map<Integer, T> stock = new HashMap<Integer, T>();
	
	//METHODES
	
	
	public Map<Integer, T> getStock() {
		return stock;
	}

	// AJOUTER UN TYPE DE PRODUIT
	public void addItem(T pItem) {
		stock.put(pItem.getId(), pItem);
	}
	
	public void addQtyAtItem(Integer itemId) {
		stock.get(itemId).addQty();
	}
	
	public T getItem(Integer itemId) {
		return stock.get(itemId);
	}
	
	public Map<Integer, T> getStockToHM(){
		return stock;
	}
	
	public T[] getStockToArray() {
		return null;
	}
	
	


}
