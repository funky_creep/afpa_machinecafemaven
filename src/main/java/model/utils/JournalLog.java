package model.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JournalLog {

	private static final Logger LOGGER = LogManager.getLogger(JournalLog.class);
	public static final int DEBUG = 0;
	public static final int INFO = 1;
	public static final int WARN = 2;
	public static final int ERROR = 3;
	
	public static void addMessage(int lvl, String message) {
		switch(lvl) {
			case DEBUG:
				LOGGER.debug(message);
				break;
			case INFO:
				LOGGER.info(message);
				break;
			case WARN:
				LOGGER.warn(message);
				break;
			case ERROR:
				LOGGER.error(message);
				break;
			default:
				break;
		}
	}
}
