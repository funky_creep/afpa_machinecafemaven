package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import exceptions.ExceptionRequest;
import model.Stockeur;
import model.beans.Piece;
import model.beans.Produit;
import model.utils.Connexion;

public class DAOPiece extends DAO<Piece> {

	
	public DAOPiece(Connexion pCxion) {
		super(pCxion);
	}

	@Override
	public Piece create(Piece objACreer) throws ExceptionRequest {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Piece update(Piece objAModifier) throws ExceptionRequest {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(int idADeleter) throws ExceptionRequest {
		// TODO Auto-generated method stub
		
	}

	/*@Override
	public void delete(Piece objADeleter) throws ExceptionRequest {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public Piece findById(int idATrouver) throws ExceptionRequest {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Stockeur<Piece> findAll() throws ExceptionRequest {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	@Override
	public Piece create(Piece objACreer) throws ExceptionCreate {
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement("INSERT INTO piece(id, nom, prix, qte) VALUES (?, ?, ?, ?);");
			ps.setInt(1, objACreer.getId());
			ps.setString(2, objACreer.getNom());
			ps.setInt(3, objACreer.getValeur());
			ps.setInt(4, objACreer.getQte());
			ps.executeUpdate();
			
			// Journalisation
			LOGGER.setLevel(Level.INFO);
			LOGGER.info("Produit avec l'id " + objACreer.getId() + " et le nom " + objACreer.getNom() + " ajout�");
			return objACreer;
		}
		catch(SQLException e) {
			LOGGER.setLevel(Level.SEVERE);
			throw new ExceptionCreate(LOGGER, objACreer.getId());
			//throw new ExceptionSQL("Cette pi�ce existe d�j� dans la base de donn�es");
		}
	}

	@Override
	public Piece update(Piece objAModifier) throws ExceptionUpdate {
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement("UPDATE piece SET nom = ?, prix = ?, qte = ? WHERE id = ?;");
			ps.setString(1, objAModifier.getNom());
			ps.setInt(2, objAModifier.getValeur());
			ps.setInt(3, objAModifier.getQte());
			ps.setInt(4, objAModifier.getId());
			ps.executeUpdate();
			
			// Journalisation
			LOGGER.setLevel(Level.INFO);
			LOGGER.info("Produit avec l'id " + objAModifier.getId() + " et le nom " + objAModifier.getNom() + " modifi�");
			return objAModifier;
		} catch (SQLException e) {
			LOGGER.setLevel(Level.SEVERE);
			throw new ExceptionUpdate(LOGGER, objAModifier.getId());
			//throw new ExceptionSQL("Cette pi�ce n'existe pas");
		}
	}

	@Override
	public void delete(int idADeleter) throws ExceptionDeleteById {
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement("DELETE FROM piece WHERE id = ?;");
			ps.setInt(1, idADeleter);
			ps.executeUpdate();
			
			// Journalisation
			LOGGER.setLevel(Level.INFO);
			LOGGER.info("Produit avec l'id " + idADeleter + " supprim�");
		} catch (SQLException e) {
			throw new ExceptionDeleteById(LOGGER, idADeleter);
			//throw new ExceptionSQL("Cette pi�ce n'existe pas");
		}
	}

	@Override
	public void delete(Piece objADeleter) throws ExceptionDeleteElement {
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement("DELETE FROM piece WHERE id = ?;");
			ps.setInt(1, objADeleter.getId());
			ps.executeUpdate();
			
			// Journalisation
			LOGGER.setLevel(Level.INFO);
			LOGGER.info("Produit avec l'id " + objADeleter.getId() + " supprim�");
		} catch (SQLException e) {
			LOGGER.setLevel(Level.SEVERE);
			throw new ExceptionDeleteElement(LOGGER, objADeleter);
			//throw new ExceptionSQL("Cette pi�ce n'existe pas");
		}
	}

	@Override
	public Piece findById(int idATrouver) throws ExceptionFindById {
		String req = "SELECT * FROM piece WHERE id = "+idATrouver;
		try {
			PreparedStatement stmt 	= getCxion().getC().prepareStatement(req);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			
			// Journalisation
			LOGGER.setLevel(Level.INFO);
			LOGGER.info("Produit avec l'id " + idATrouver + " trouv�");
			return new Piece(rs.getInt("id"), rs.getString("nom"), rs.getInt("prix"), rs.getInt("qte"));					
		} catch (SQLException e) {
			LOGGER.setLevel(Level.SEVERE);
			throw new ExceptionFindById(LOGGER, idATrouver);
			//throw new ExceptionSQL("Cette pi�ce n'existe pas");
		}
	}

	@Override
	public Stockeur<Piece> findAll() throws SQLException {
		Stockeur<Piece> stockPieces = new Stockeur<Piece>();
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement("SELECT * from piece;");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				stockPieces.addItem(new Piece(rs.getInt("id"), rs.getString("nom"), rs.getInt("prix"), rs.getInt("qte")));
			}
			
			// Journalisation
			LOGGER.setLevel(Level.INFO);
			LOGGER.info("Produits trouv�s");
			return stockPieces;
		} catch (SQLException e) {
			throw new SQLException();
			//throw new ExceptionSQL("Cette table n'existe pas");
		}
	}*/

}
