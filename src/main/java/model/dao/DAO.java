package model.dao;

import java.sql.SQLException;
import java.util.logging.Logger;

import exceptions.ExceptionRequest;
import model.Stockable;
import model.Stockeur;
import model.beans.Produit;
import model.utils.Connexion;

public abstract class DAO<T extends Stockable> {
	
	private Connexion cxion = null;
	
	/**
	 * CONSTRUCTEUR
	 * 
	 * @param cxion
	 */
	public DAO(Connexion pCxion) {
		this.cxion = pCxion;
	}
	
	/**
	 * GETTERS & SETTERS
	 */
	
	protected Connexion getCxion() {
		return this.cxion;
	}
	
	

	/**
	 * 
	 * METHODES
	 * @throws ExceptionSQL 
	 * @throws ExceptionCreate 
	 * @throws ExceptionRequest 
	 * 
	 */
	
	public abstract T 			create(T 	objACreer) throws ExceptionRequest;
	public abstract T 			update(T 	objAModifier) throws ExceptionRequest;
	public abstract void 		delete(int 	idADeleter) throws ExceptionRequest;	
	public abstract T 			findById(int idATrouver) throws ExceptionRequest;
	public abstract Stockeur<T> findAll() throws ExceptionRequest;
	
}
